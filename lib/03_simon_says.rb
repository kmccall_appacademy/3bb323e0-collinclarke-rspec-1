def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num = 2)
  ((string + ' ') * num)[0...-1]
end

def start_of_word(string, num)
  string[0...num]
end

def first_word(string)
  string.split.first
end

def titleize(string)
  inferior_words = ['and', 'the', 'of', 'over']
  string.split.map.with_index do |word, idx|
    if idx == 0
      word[0].upcase + word[1..-1]
    elsif inferior_words.include?(word)
      word
    else
     word[0].upcase + word[1..-1]
    end
  end.join(' ')
end
