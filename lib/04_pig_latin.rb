
def latinize(word)
  vowels = 'aeio'
  chunk = []
  letters = word.chars
    letters.each_with_index do |letter, idx|
      if vowels.include?(letter)
        return word[idx..-1] + chunk.join('') + 'ay'
      else
        chunk << letter
      end
    end
end

def translate(string)
  string.split(' ').map do |word|
    latinize(word)
  end.join(' ')
end
